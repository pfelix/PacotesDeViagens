﻿namespace PacotesDeViagens
{
    using ClassLibrary;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    public partial class FormLerPacote : Form
    {
        private List<Pacote> ListaDePacotes;

        public FormLerPacote(List<Pacote> ListaDePacotes)
        {
            InitializeComponent();
            this.ListaDePacotes = ListaDePacotes;

            for (int i = 0; i < ListaDePacotes.Count; i++)
            {
                DataGridViewListaPacotes.Rows.Add(
                    ListaDePacotes[i].IdPacote.ToString(),
                    ListaDePacotes[i].Descricao,
                    ListaDePacotes[i].Preco
                    );
            }
        }

        private void ButtonSair_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
