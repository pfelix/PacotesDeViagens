﻿namespace PacotesDeViagens
{
    using ClassLibrary;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    public partial class FormEditarPacote : Form
    {
        List<Pacote> ListaDePacotes;
        int index = 0;

        public FormEditarPacote(List<Pacote> ListaDePacotes)
        {
            InitializeComponent();
            this.ListaDePacotes = ListaDePacotes;



            MostarDados(index);
            
        }

        private void ButtonCancelar_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void MostarDados (int index)
        {
            TextBoxIdPacote.Text = ListaDePacotes[index].IdPacote.ToString();
            TextBoxDescricao.Text = ListaDePacotes[index].Descricao;
            NumericUpDownPreco.Value = Convert.ToDecimal(ListaDePacotes[index].Preco);
        }

        private void ButtonFrente_Click(object sender, EventArgs e)
        {
            index += 1;
            if (index > (ListaDePacotes.Count - 1))
            {
                index = 0;
            }

            MostarDados(index);
        }

        private void ButtonTras_Click(object sender, EventArgs e)
        {
            index -= 1;
            if (index < 0)
            {
                index = ListaDePacotes.Count - 1;
            }

            MostarDados(index);
        }

        private void ButtonUltimo_Click(object sender, EventArgs e)
        {
            index = ListaDePacotes.Count - 1;
            MostarDados(index);
        }

        private void ButtonPrimeiro_Click(object sender, EventArgs e)
        {
            index = 0;
            MostarDados(index);
        }

        private void ButtonSalvar_Click(object sender, EventArgs e)
        {
            ListaDePacotes[index].Descricao = TextBoxDescricao.Text;
            ListaDePacotes[index].Preco = Convert.ToDouble(NumericUpDownPreco.Value);
            MessageBox.Show("Pacote aletrado com sucesso.");
        }
    }
}
