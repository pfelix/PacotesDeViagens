﻿namespace PacotesDeViagens
{
    using ClassLibrary;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Windows.Forms;

    public partial class FormPrincipal : Form
    {
        private FormCriarPacote formCriarPacote;
        private FormLerPacote formLerPacote;
        private FormApagarPacote formApagarPacote;
        private FormEditarPacote formEditarPacote;

        private string destinoFicheiro = null;

        List<Pacote> ListaDePacotes = ListaPacotes.LoadPacotes();

        public FormPrincipal()
        {
            InitializeComponent();

            bloquiarSalvar();
        }
 

        #region Botões

        private void ButtonFechar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ButtonCriar_Click(object sender, EventArgs e)
        {
            formCriarPacote = new FormCriarPacote(ListaDePacotes);
            formCriarPacote.Show();
        }

        private void ButtonLer_Click(object sender, EventArgs e)
        {
            if (ListaDePacotes.Count == 0)
            {
                MessageBox.Show("Não existem dados para ler.");
                return;
            }

            formLerPacote = new FormLerPacote(ListaDePacotes);
            formLerPacote.Show();
        }

        private void ButtonApagar_Click(object sender, EventArgs e)
        {
            if (ListaDePacotes.Count == 0)
            {
                MessageBox.Show("Não existem dados para apagar.");
                return;
            }

            formApagarPacote = new FormApagarPacote(ListaDePacotes);
            formApagarPacote.Show();
        }

        private void ButtonAtualizar_Click(object sender, EventArgs e)
        {
            if (ListaDePacotes.Count == 0)
            {
                MessageBox.Show("Não existem dados para alterar.");
                return;
            }

            formEditarPacote = new FormEditarPacote(ListaDePacotes);
            formEditarPacote.Show();
        }

        #endregion

        #region Menu

        private void sobreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Criado por: Paulo Félix. \nData 08/10/2017 \nPacote de Viagens v1.0.0");
        }

        private void salvarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            salvarComo();
            bloquiarSalvar();
        }

        private void ficheiroToolStripMenuItem_Click(object sender, EventArgs e)
        {

            StreamReader myStream;
            OpenFileDialog ficheiro = new OpenFileDialog();
            //ficheiro.InitialDirectory = "c:\\";
            ficheiro.Filter = "Text file|*.txt";
            //ficheiro.FilterIndex = 2;
            //ficheiro.RestoreDirectory = true;

            if (ficheiro.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = new StreamReader(ficheiro.OpenFile())) != null)
                    {
                        destinoFicheiro = ficheiro.FileName;
                        using (myStream)
                        {
                            ListaDePacotes.Clear();
                            string linha = "";

                            while ((linha = myStream.ReadLine()) != null)
                            {
                                string[] campos = new string[3];
                                campos = linha.Split(';');

                                var pacote = new Pacote
                                {
                                    IdPacote = Convert.ToInt32(campos[0]),
                                    Descricao = campos[1],
                                    Preco = Convert.ToDouble(campos[2])
                                };

                                ListaDePacotes.Add(pacote);
                            }

                            myStream.Close();
                        }
                    }
                    bloquiarSalvar();
                }
                catch (Exception erro)
                {
                    MessageBox.Show("Não foi possivel ler o ficheiro.\nErro: " + erro.Message);
                }
            }
        }

        private void salvarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (destinoFicheiro != null)
            {

                StreamWriter sw = new StreamWriter(destinoFicheiro, false);

                try // Try {} catch (Exception e) para fazer verificações com acessos exterior
                {
                    if (!File.Exists(destinoFicheiro))
                    {
                        sw = File.CreateText(destinoFicheiro);
                    }

                    foreach (var pacotes in ListaDePacotes)
                    {
                        string linha = string.Format("{0};{1};{2}", pacotes.IdPacote, pacotes.Descricao, pacotes.Preco);
                        sw.WriteLine(linha);
                    }

                    sw.Close();
                }
                catch (Exception erro)
                {
                    MessageBox.Show(erro.Message);
                }

                return;
            }
            salvarComo();


        }
        #endregion


        private void salvarComo()
        {
            #region Aparece janela para utilizador introduzir nome e diretoria
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Ficheiro de texto|*.txt"; // Formatos disponiveis
            saveFileDialog1.Title = "Salvar como...";
            saveFileDialog1.ShowDialog();
            #endregion

            if (saveFileDialog1.FileName != "")
            {
                FileStream fs = (FileStream)saveFileDialog1.OpenFile(); // Cria o ficheiro
                
                StreamWriter sw = new StreamWriter(fs); // Abre a opção de escrita da linha
                
                destinoFicheiro = saveFileDialog1.FileName;

                foreach (var pacotes in ListaDePacotes)
                {
                    string linha = string.Format("{0};{1};{2}", pacotes.IdPacote, pacotes.Descricao, pacotes.Preco);
                    sw.WriteLine(linha); // escreve no ficheiro cada linha
                }
                sw.Close(); // fecha a escrita

               fs.Close(); // fecha o ficheiro
            }
        }

        private void bloquiarSalvar()
        {
            if (destinoFicheiro == null)
            {
                salvarToolStripMenuItem1.Enabled = false; //Opção Salvar
            }
            else
            {
                salvarToolStripMenuItem1.Enabled = true; //Opção Salvar
            }
        }


    }
}
