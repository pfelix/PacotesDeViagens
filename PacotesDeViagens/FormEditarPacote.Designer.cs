﻿namespace PacotesDeViagens
{
    partial class FormEditarPacote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.LabelIdPacote = new System.Windows.Forms.Label();
            this.TextBoxIdPacote = new System.Windows.Forms.TextBox();
            this.LabelDescricao = new System.Windows.Forms.Label();
            this.LabelPreco = new System.Windows.Forms.Label();
            this.TextBoxDescricao = new System.Windows.Forms.TextBox();
            this.NumericUpDownPreco = new System.Windows.Forms.NumericUpDown();
            this.ButtonCancelar = new System.Windows.Forms.Button();
            this.ButtonSalvar = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.ButtonFrente = new System.Windows.Forms.Button();
            this.ButtonTras = new System.Windows.Forms.Button();
            this.ButtonUltimo = new System.Windows.Forms.Button();
            this.ButtonPrimeiro = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownPreco)).BeginInit();
            this.SuspendLayout();
            // 
            // LabelIdPacote
            // 
            this.LabelIdPacote.AutoSize = true;
            this.LabelIdPacote.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelIdPacote.Location = new System.Drawing.Point(13, 15);
            this.LabelIdPacote.Name = "LabelIdPacote";
            this.LabelIdPacote.Size = new System.Drawing.Size(61, 13);
            this.LabelIdPacote.TabIndex = 0;
            this.LabelIdPacote.Text = "Id pacote";
            // 
            // TextBoxIdPacote
            // 
            this.TextBoxIdPacote.Location = new System.Drawing.Point(81, 11);
            this.TextBoxIdPacote.Name = "TextBoxIdPacote";
            this.TextBoxIdPacote.ReadOnly = true;
            this.TextBoxIdPacote.Size = new System.Drawing.Size(68, 20);
            this.TextBoxIdPacote.TabIndex = 1;
            // 
            // LabelDescricao
            // 
            this.LabelDescricao.AutoSize = true;
            this.LabelDescricao.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelDescricao.Location = new System.Drawing.Point(10, 41);
            this.LabelDescricao.Name = "LabelDescricao";
            this.LabelDescricao.Size = new System.Drawing.Size(64, 13);
            this.LabelDescricao.TabIndex = 3;
            this.LabelDescricao.Text = "Descrição";
            // 
            // LabelPreco
            // 
            this.LabelPreco.AutoSize = true;
            this.LabelPreco.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelPreco.Location = new System.Drawing.Point(34, 144);
            this.LabelPreco.Name = "LabelPreco";
            this.LabelPreco.Size = new System.Drawing.Size(40, 13);
            this.LabelPreco.TabIndex = 4;
            this.LabelPreco.Text = "Preço";
            // 
            // TextBoxDescricao
            // 
            this.TextBoxDescricao.Location = new System.Drawing.Point(81, 38);
            this.TextBoxDescricao.Multiline = true;
            this.TextBoxDescricao.Name = "TextBoxDescricao";
            this.TextBoxDescricao.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TextBoxDescricao.Size = new System.Drawing.Size(249, 94);
            this.TextBoxDescricao.TabIndex = 6;
            // 
            // NumericUpDownPreco
            // 
            this.NumericUpDownPreco.DecimalPlaces = 2;
            this.NumericUpDownPreco.Location = new System.Drawing.Point(81, 140);
            this.NumericUpDownPreco.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.NumericUpDownPreco.Name = "NumericUpDownPreco";
            this.NumericUpDownPreco.Size = new System.Drawing.Size(120, 20);
            this.NumericUpDownPreco.TabIndex = 7;
            this.NumericUpDownPreco.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // ButtonCancelar
            // 
            this.ButtonCancelar.Image = global::PacotesDeViagens.Properties.Resources.ic_cancel;
            this.ButtonCancelar.Location = new System.Drawing.Point(287, 172);
            this.ButtonCancelar.Name = "ButtonCancelar";
            this.ButtonCancelar.Size = new System.Drawing.Size(45, 45);
            this.ButtonCancelar.TabIndex = 8;
            this.toolTip1.SetToolTip(this.ButtonCancelar, "Sair");
            this.ButtonCancelar.UseVisualStyleBackColor = true;
            this.ButtonCancelar.Click += new System.EventHandler(this.ButtonCancelar_Click);
            // 
            // ButtonSalvar
            // 
            this.ButtonSalvar.AccessibleName = "";
            this.ButtonSalvar.Image = global::PacotesDeViagens.Properties.Resources.ic_save;
            this.ButtonSalvar.Location = new System.Drawing.Point(236, 172);
            this.ButtonSalvar.Name = "ButtonSalvar";
            this.ButtonSalvar.Size = new System.Drawing.Size(45, 45);
            this.ButtonSalvar.TabIndex = 9;
            this.toolTip1.SetToolTip(this.ButtonSalvar, "Alterar");
            this.ButtonSalvar.UseVisualStyleBackColor = true;
            this.ButtonSalvar.Click += new System.EventHandler(this.ButtonSalvar_Click);
            // 
            // ButtonFrente
            // 
            this.ButtonFrente.Image = global::PacotesDeViagens.Properties.Resources.ic_forward;
            this.ButtonFrente.Location = new System.Drawing.Point(134, 172);
            this.ButtonFrente.Name = "ButtonFrente";
            this.ButtonFrente.Size = new System.Drawing.Size(45, 45);
            this.ButtonFrente.TabIndex = 10;
            this.toolTip1.SetToolTip(this.ButtonFrente, "Frente");
            this.ButtonFrente.UseVisualStyleBackColor = true;
            this.ButtonFrente.Click += new System.EventHandler(this.ButtonFrente_Click);
            // 
            // ButtonTras
            // 
            this.ButtonTras.Image = global::PacotesDeViagens.Properties.Resources.ic_back;
            this.ButtonTras.Location = new System.Drawing.Point(83, 172);
            this.ButtonTras.Name = "ButtonTras";
            this.ButtonTras.Size = new System.Drawing.Size(45, 45);
            this.ButtonTras.TabIndex = 11;
            this.toolTip1.SetToolTip(this.ButtonTras, "Trás");
            this.ButtonTras.UseVisualStyleBackColor = true;
            this.ButtonTras.Click += new System.EventHandler(this.ButtonTras_Click);
            // 
            // ButtonUltimo
            // 
            this.ButtonUltimo.Image = global::PacotesDeViagens.Properties.Resources.ic_last;
            this.ButtonUltimo.Location = new System.Drawing.Point(185, 172);
            this.ButtonUltimo.Name = "ButtonUltimo";
            this.ButtonUltimo.Size = new System.Drawing.Size(45, 45);
            this.ButtonUltimo.TabIndex = 12;
            this.toolTip1.SetToolTip(this.ButtonUltimo, "Ultimo");
            this.ButtonUltimo.UseVisualStyleBackColor = true;
            this.ButtonUltimo.Click += new System.EventHandler(this.ButtonUltimo_Click);
            // 
            // ButtonPrimeiro
            // 
            this.ButtonPrimeiro.Image = global::PacotesDeViagens.Properties.Resources.ic_first;
            this.ButtonPrimeiro.Location = new System.Drawing.Point(32, 172);
            this.ButtonPrimeiro.Name = "ButtonPrimeiro";
            this.ButtonPrimeiro.Size = new System.Drawing.Size(45, 45);
            this.ButtonPrimeiro.TabIndex = 13;
            this.toolTip1.SetToolTip(this.ButtonPrimeiro, "Primeiro");
            this.ButtonPrimeiro.UseVisualStyleBackColor = true;
            this.ButtonPrimeiro.Click += new System.EventHandler(this.ButtonPrimeiro_Click);
            // 
            // FormEditarPacote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 231);
            this.ControlBox = false;
            this.Controls.Add(this.ButtonPrimeiro);
            this.Controls.Add(this.ButtonUltimo);
            this.Controls.Add(this.ButtonTras);
            this.Controls.Add(this.ButtonFrente);
            this.Controls.Add(this.ButtonSalvar);
            this.Controls.Add(this.ButtonCancelar);
            this.Controls.Add(this.NumericUpDownPreco);
            this.Controls.Add(this.TextBoxDescricao);
            this.Controls.Add(this.LabelPreco);
            this.Controls.Add(this.LabelDescricao);
            this.Controls.Add(this.TextBoxIdPacote);
            this.Controls.Add(this.LabelIdPacote);
            this.Name = "FormEditarPacote";
            this.Text = "Editar pacote";
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownPreco)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelIdPacote;
        private System.Windows.Forms.TextBox TextBoxIdPacote;
        private System.Windows.Forms.Label LabelDescricao;
        private System.Windows.Forms.Label LabelPreco;
        private System.Windows.Forms.TextBox TextBoxDescricao;
        private System.Windows.Forms.NumericUpDown NumericUpDownPreco;
        private System.Windows.Forms.Button ButtonCancelar;
        private System.Windows.Forms.Button ButtonSalvar;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button ButtonFrente;
        private System.Windows.Forms.Button ButtonTras;
        private System.Windows.Forms.Button ButtonUltimo;
        private System.Windows.Forms.Button ButtonPrimeiro;
    }
}