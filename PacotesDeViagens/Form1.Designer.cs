﻿namespace PacotesDeViagens
{
    partial class FormPrincipal
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.configuraçõesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ficheiroToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salvarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.salvarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sobreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.ButtonFechar = new System.Windows.Forms.Button();
            this.ButtonApagar = new System.Windows.Forms.Button();
            this.ButtonLer = new System.Windows.Forms.Button();
            this.ButtonAtualizar = new System.Windows.Forms.Button();
            this.ButtonCriar = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.configuraçõesToolStripMenuItem,
            this.sobreToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(405, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(12, 20);
            // 
            // configuraçõesToolStripMenuItem
            // 
            this.configuraçõesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ficheiroToolStripMenuItem,
            this.salvarToolStripMenuItem1,
            this.salvarToolStripMenuItem});
            this.configuraçõesToolStripMenuItem.Name = "configuraçõesToolStripMenuItem";
            this.configuraçõesToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.configuraçõesToolStripMenuItem.Text = "Ficheiro";
            // 
            // ficheiroToolStripMenuItem
            // 
            this.ficheiroToolStripMenuItem.Name = "ficheiroToolStripMenuItem";
            this.ficheiroToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.ficheiroToolStripMenuItem.Text = "Abrir";
            this.ficheiroToolStripMenuItem.Click += new System.EventHandler(this.ficheiroToolStripMenuItem_Click);
            // 
            // salvarToolStripMenuItem1
            // 
            this.salvarToolStripMenuItem1.Name = "salvarToolStripMenuItem1";
            this.salvarToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.salvarToolStripMenuItem1.Text = "Salvar";
            this.salvarToolStripMenuItem1.Click += new System.EventHandler(this.salvarToolStripMenuItem1_Click);
            // 
            // salvarToolStripMenuItem
            // 
            this.salvarToolStripMenuItem.Name = "salvarToolStripMenuItem";
            this.salvarToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.salvarToolStripMenuItem.Text = "Salvar como...";
            this.salvarToolStripMenuItem.Click += new System.EventHandler(this.salvarToolStripMenuItem_Click);
            // 
            // sobreToolStripMenuItem
            // 
            this.sobreToolStripMenuItem.Name = "sobreToolStripMenuItem";
            this.sobreToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.sobreToolStripMenuItem.Text = "Sobre...";
            this.sobreToolStripMenuItem.Click += new System.EventHandler(this.sobreToolStripMenuItem_Click);
            // 
            // ButtonFechar
            // 
            this.ButtonFechar.Image = global::PacotesDeViagens.Properties.Resources.ic_exit;
            this.ButtonFechar.Location = new System.Drawing.Point(348, 222);
            this.ButtonFechar.Name = "ButtonFechar";
            this.ButtonFechar.Size = new System.Drawing.Size(45, 45);
            this.ButtonFechar.TabIndex = 4;
            this.toolTip1.SetToolTip(this.ButtonFechar, "Sair");
            this.ButtonFechar.UseVisualStyleBackColor = true;
            this.ButtonFechar.Click += new System.EventHandler(this.ButtonFechar_Click);
            // 
            // ButtonApagar
            // 
            this.ButtonApagar.Image = global::PacotesDeViagens.Properties.Resources.ic_deleted;
            this.ButtonApagar.Location = new System.Drawing.Point(238, 154);
            this.ButtonApagar.Name = "ButtonApagar";
            this.ButtonApagar.Size = new System.Drawing.Size(75, 55);
            this.ButtonApagar.TabIndex = 3;
            this.toolTip1.SetToolTip(this.ButtonApagar, "Apagar");
            this.ButtonApagar.UseVisualStyleBackColor = true;
            this.ButtonApagar.Click += new System.EventHandler(this.ButtonApagar_Click);
            // 
            // ButtonLer
            // 
            this.ButtonLer.Image = global::PacotesDeViagens.Properties.Resources.ic_read;
            this.ButtonLer.Location = new System.Drawing.Point(238, 54);
            this.ButtonLer.Name = "ButtonLer";
            this.ButtonLer.Size = new System.Drawing.Size(75, 55);
            this.ButtonLer.TabIndex = 2;
            this.toolTip1.SetToolTip(this.ButtonLer, "Ler");
            this.ButtonLer.UseVisualStyleBackColor = true;
            this.ButtonLer.Click += new System.EventHandler(this.ButtonLer_Click);
            // 
            // ButtonAtualizar
            // 
            this.ButtonAtualizar.Image = global::PacotesDeViagens.Properties.Resources.ic_updade;
            this.ButtonAtualizar.Location = new System.Drawing.Point(80, 154);
            this.ButtonAtualizar.Name = "ButtonAtualizar";
            this.ButtonAtualizar.Size = new System.Drawing.Size(75, 55);
            this.ButtonAtualizar.TabIndex = 1;
            this.toolTip1.SetToolTip(this.ButtonAtualizar, "Editar");
            this.ButtonAtualizar.UseVisualStyleBackColor = true;
            this.ButtonAtualizar.Click += new System.EventHandler(this.ButtonAtualizar_Click);
            // 
            // ButtonCriar
            // 
            this.ButtonCriar.Image = global::PacotesDeViagens.Properties.Resources.ic_creat;
            this.ButtonCriar.Location = new System.Drawing.Point(80, 54);
            this.ButtonCriar.Name = "ButtonCriar";
            this.ButtonCriar.Size = new System.Drawing.Size(75, 55);
            this.ButtonCriar.TabIndex = 0;
            this.toolTip1.SetToolTip(this.ButtonCriar, "Criar");
            this.ButtonCriar.UseVisualStyleBackColor = true;
            this.ButtonCriar.Click += new System.EventHandler(this.ButtonCriar_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // FormPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 279);
            this.ControlBox = false;
            this.Controls.Add(this.ButtonFechar);
            this.Controls.Add(this.ButtonApagar);
            this.Controls.Add(this.ButtonLer);
            this.Controls.Add(this.ButtonAtualizar);
            this.Controls.Add(this.ButtonCriar);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormPrincipal";
            this.Text = "Pacotes de viagens";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonCriar;
        private System.Windows.Forms.Button ButtonAtualizar;
        private System.Windows.Forms.Button ButtonLer;
        private System.Windows.Forms.Button ButtonApagar;
        private System.Windows.Forms.Button ButtonFechar;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem sobreToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem configuraçõesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ficheiroToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salvarToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem salvarToolStripMenuItem1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

