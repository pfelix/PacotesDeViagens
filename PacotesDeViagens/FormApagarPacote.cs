﻿namespace PacotesDeViagens
{
    using ClassLibrary;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    public partial class FormApagarPacote : Form
    {
        List<Pacote> ListaDePacotes;

        public FormApagarPacote(List<Pacote> ListaDePacotes)
        {
            InitializeComponent();
            this.ListaDePacotes = ListaDePacotes;

            ComboBoxListaDePacotes.DataSource = ListaDePacotes;
        }

        private void ButtonCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ButtonSalvar_Click(object sender, EventArgs e)
        {
            //Pacote pacoteSelecionado = (Pacote)ComboBoxListaDePacotes.SelectedItem;

            int index;

            index = ComboBoxListaDePacotes.SelectedIndex;

            if (index == -1)
            {
                MessageBox.Show("Não escolheu um pacote para apagar.");
                return;
            }

            if (MessageBox.Show("Tem a certeza que pretende apagar o seguinte pacote:\nId Pacote: "+ ListaDePacotes[index].IdPacote.ToString()+
                "\nDescrição: "+ ListaDePacotes[index].Descricao+
                "\nPreço: "+ ListaDePacotes[index].Preco.ToString(),
                "",
                MessageBoxButtons.YesNo
                ) == DialogResult.Yes)
            {
                ListaDePacotes.RemoveAt(index);
                MessageBox.Show("O pacote foi apagado.");

                ComboBoxListaDePacotes.DataSource = null;
                ComboBoxListaDePacotes.DataSource = ListaDePacotes;

                if (ListaDePacotes.Count == 0)
                {
                    MessageBox.Show("Não existem mais pacotes para apagar.");
                    Close();
                }
                return;
            }
        }
    }
}
