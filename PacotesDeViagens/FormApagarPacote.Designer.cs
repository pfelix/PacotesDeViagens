﻿namespace PacotesDeViagens
{
    partial class FormApagarPacote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.LabelListaDePacotes = new System.Windows.Forms.Label();
            this.ComboBoxListaDePacotes = new System.Windows.Forms.ComboBox();
            this.ButtonSalvar = new System.Windows.Forms.Button();
            this.ButtonCancelar = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // LabelListaDePacotes
            // 
            this.LabelListaDePacotes.AutoSize = true;
            this.LabelListaDePacotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelListaDePacotes.Location = new System.Drawing.Point(12, 54);
            this.LabelListaDePacotes.Name = "LabelListaDePacotes";
            this.LabelListaDePacotes.Size = new System.Drawing.Size(105, 13);
            this.LabelListaDePacotes.TabIndex = 0;
            this.LabelListaDePacotes.Text = "Lista de pacotes:";
            // 
            // ComboBoxListaDePacotes
            // 
            this.ComboBoxListaDePacotes.FormattingEnabled = true;
            this.ComboBoxListaDePacotes.Location = new System.Drawing.Point(123, 50);
            this.ComboBoxListaDePacotes.Name = "ComboBoxListaDePacotes";
            this.ComboBoxListaDePacotes.Size = new System.Drawing.Size(236, 21);
            this.ComboBoxListaDePacotes.TabIndex = 1;
            // 
            // ButtonSalvar
            // 
            this.ButtonSalvar.Image = global::PacotesDeViagens.Properties.Resources.ic_ok;
            this.ButtonSalvar.Location = new System.Drawing.Point(240, 95);
            this.ButtonSalvar.Name = "ButtonSalvar";
            this.ButtonSalvar.Size = new System.Drawing.Size(45, 45);
            this.ButtonSalvar.TabIndex = 2;
            this.toolTip1.SetToolTip(this.ButtonSalvar, "Apagar");
            this.ButtonSalvar.UseVisualStyleBackColor = true;
            this.ButtonSalvar.Click += new System.EventHandler(this.ButtonSalvar_Click);
            // 
            // ButtonCancelar
            // 
            this.ButtonCancelar.Image = global::PacotesDeViagens.Properties.Resources.ic_cancel;
            this.ButtonCancelar.Location = new System.Drawing.Point(314, 95);
            this.ButtonCancelar.Name = "ButtonCancelar";
            this.ButtonCancelar.Size = new System.Drawing.Size(45, 45);
            this.ButtonCancelar.TabIndex = 3;
            this.toolTip1.SetToolTip(this.ButtonCancelar, "Sair");
            this.ButtonCancelar.UseVisualStyleBackColor = true;
            this.ButtonCancelar.Click += new System.EventHandler(this.ButtonCancelar_Click);
            // 
            // FormApagarPacote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 158);
            this.ControlBox = false;
            this.Controls.Add(this.ButtonCancelar);
            this.Controls.Add(this.ButtonSalvar);
            this.Controls.Add(this.ComboBoxListaDePacotes);
            this.Controls.Add(this.LabelListaDePacotes);
            this.Name = "FormApagarPacote";
            this.Text = "Apagar pacote";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelListaDePacotes;
        private System.Windows.Forms.ComboBox ComboBoxListaDePacotes;
        private System.Windows.Forms.Button ButtonSalvar;
        private System.Windows.Forms.Button ButtonCancelar;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}