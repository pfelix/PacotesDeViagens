﻿namespace PacotesDeViagens
{
    partial class FormLerPacote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.DataGridViewListaPacotes = new System.Windows.Forms.DataGridView();
            this.ColumnIdPacote = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDescricao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPreco = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ButtonSair = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewListaPacotes)).BeginInit();
            this.SuspendLayout();
            // 
            // DataGridViewListaPacotes
            // 
            this.DataGridViewListaPacotes.AllowUserToAddRows = false;
            this.DataGridViewListaPacotes.AllowUserToDeleteRows = false;
            this.DataGridViewListaPacotes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataGridViewListaPacotes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewListaPacotes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnIdPacote,
            this.ColumnDescricao,
            this.ColumnPreco});
            this.DataGridViewListaPacotes.Location = new System.Drawing.Point(13, 13);
            this.DataGridViewListaPacotes.Name = "DataGridViewListaPacotes";
            this.DataGridViewListaPacotes.ReadOnly = true;
            this.DataGridViewListaPacotes.Size = new System.Drawing.Size(419, 204);
            this.DataGridViewListaPacotes.TabIndex = 0;
            // 
            // ColumnIdPacote
            // 
            this.ColumnIdPacote.FillWeight = 50F;
            this.ColumnIdPacote.HeaderText = "Id Pacote";
            this.ColumnIdPacote.Name = "ColumnIdPacote";
            this.ColumnIdPacote.ReadOnly = true;
            // 
            // ColumnDescricao
            // 
            this.ColumnDescricao.HeaderText = "Descrição";
            this.ColumnDescricao.Name = "ColumnDescricao";
            this.ColumnDescricao.ReadOnly = true;
            // 
            // ColumnPreco
            // 
            dataGridViewCellStyle1.Format = "N2";
            dataGridViewCellStyle1.NullValue = null;
            this.ColumnPreco.DefaultCellStyle = dataGridViewCellStyle1;
            this.ColumnPreco.FillWeight = 50F;
            this.ColumnPreco.HeaderText = "Preço";
            this.ColumnPreco.Name = "ColumnPreco";
            this.ColumnPreco.ReadOnly = true;
            // 
            // ButtonSair
            // 
            this.ButtonSair.Image = global::PacotesDeViagens.Properties.Resources.ic_exit;
            this.ButtonSair.Location = new System.Drawing.Point(387, 223);
            this.ButtonSair.Name = "ButtonSair";
            this.ButtonSair.Size = new System.Drawing.Size(45, 45);
            this.ButtonSair.TabIndex = 1;
            this.toolTip1.SetToolTip(this.ButtonSair, "Sair");
            this.ButtonSair.UseVisualStyleBackColor = true;
            this.ButtonSair.Click += new System.EventHandler(this.ButtonSair_Click);
            // 
            // FormLerPacote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 273);
            this.ControlBox = false;
            this.Controls.Add(this.ButtonSair);
            this.Controls.Add(this.DataGridViewListaPacotes);
            this.Name = "FormLerPacote";
            this.Text = "Ler pacote";
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewListaPacotes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView DataGridViewListaPacotes;
        private System.Windows.Forms.Button ButtonSair;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnIdPacote;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDescricao;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnPreco;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}