﻿namespace PacotesDeViagens
{
    using ClassLibrary;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;


    public partial class FormCriarPacote : Form
    {
        private List<Pacote> ListaDePacotes;
        private readonly Pacote myPacote;
        int novoId;

        public FormCriarPacote(List<Pacote> ListaDePacotes)
        {
            InitializeComponent();
            this.ListaDePacotes = ListaDePacotes;
            myPacote = new Pacote();

            if (ListaDePacotes.Count - 1 < 0)
            {
                novoId = 1;
            }
            else
            {
                novoId = myPacote.GerarId(ListaDePacotes[ListaDePacotes.Count - 1].IdPacote); // Gera um novo ID
            }

            TextBoxIdPacote.Text = novoId.ToString();
        }

        private void ButtonCancelar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(TextBoxDescricao.Text))
            {
                DialogResult dialogo = MessageBox.Show("Os dados vão ser perdidos\nDeseja continuar?", "Atenção", MessageBoxButtons.YesNo);
                switch (dialogo)
                {
                    case DialogResult.Yes:
                        Close();
                        break;
                    case DialogResult.No:
                        break;
                }
                return;
            }

            Close();
        }

        private void ButtonSalvar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TextBoxDescricao.Text))
            {
                MessageBox.Show("Falta introduzir uma descrição para o pacote.");
                return;
            }

            if (NumericUpDownPreco.Value <= 0)
            {
                MessageBox.Show("Falta intorduzir um preço para o pacote.");
                return;
            }


            ListaDePacotes.Add(new Pacote
            {
                IdPacote = novoId,
                Descricao = TextBoxDescricao.Text,
                Preco = Convert.ToDouble(NumericUpDownPreco.Value)
            });

            MessageBox.Show("Pacote gravado com sucesso");
            Close();
        }
    }
}

