﻿namespace ClassLibrary
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class ListaPacotes
    {
        public static List<Pacote> LoadPacotes()
        {
            List<Pacote> output = new List<Pacote>();

            output.Add(new Pacote { IdPacote = 1, Descricao = "Pacote 1.", Preco = 200.00 });
            output.Add(new Pacote { IdPacote = 2, Descricao = "Pacote 2.", Preco = 300.00 });
            output.Add(new Pacote { IdPacote = 3, Descricao = "Pacote 3.", Preco = 600.00 });
            output.Add(new Pacote { IdPacote = 4, Descricao = "Pacote 4.", Preco = 700.00 });

            return output;

        }

    }
}
