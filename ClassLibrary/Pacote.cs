﻿namespace ClassLibrary
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class Pacote
    {
        #region Atributos

        private int _idPacote;
        private string _descricao;
        private double _preco;

        #endregion

        #region Prorpiedade

        public int IdPacote { get; set; }
        public string Descricao { get; set; }
        public double Preco { get; set; }

        #endregion

        #region Construtores

        //Default

        public Pacote() : this(1, "Descrição 1", 0) { }

        //Parametros
        public Pacote(int idPacote, string descricao, double preco)
        {
            IdPacote = idPacote;
            Descricao = descricao;
            Preco = preco;
        }

        //Cópia
        public Pacote(Pacote pacote) : this(pacote.IdPacote, pacote.Descricao, pacote.Preco) { }

        #endregion

        #region Metodos - Gerais

        public override string ToString()
        {
            return string.Format("Id Pacote: {0}  Descrição: {1}  Preço: {2}", IdPacote, Descricao, Preco);
        }

        public override int GetHashCode()
        {
            return IdPacote;
        }

        #endregion

        #region Metodos - Outros

        public int GerarId(int ultimoId)
        {
            return ultimoId + 1;
        }

        #endregion

    }
}
